import boto3, pytest

s3 = boto3.resource('s3')
ec2 = boto3.resource('ec2')
ec2_client = boto3.client('ec2')

for bucket in s3.buckets.all():
    print (bucket.name)

ec2_client.describe_instances()

response = ec2.create_instances(ImageId='ami-0bbc25e23a7640b9b', InstanceType='t2.nano', MinCount=1, MaxCount=1)
ids = []
ids.append(response[0].instance_id)

response[0].wait_until_running()

def test_instanceExists():
    status = False
    test1 = ec2.instances.filter(
        Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
    for instance in test1:
        if instance.id in ids:
            status = True
            assert status == True
    assert status == False
            
ec2.instances.filter(InstanceIds=ids).terminate()

def test_instanceShutDown():
    status = False
    test2 = ec2.instances.filter(
        Filters=[{'Name': 'instance-state-name', 'Values': ['shutting-down']}])
    for instance in test2:
        if instance.id in ids:
            status = True   
            assert status == True
    assert status == True

# response[0].wait_until_terminated()

#def test_instanceTerminated():
#    status = False
#    test3 = ec2.instances.filter(
#        Filters=[{'Name': 'instance-state-name', 'Values': ['terminated']}])
#    for instance in test3:
#        if instance.id in ids:
#            status = True   
#            assert status == True
#    assert status == True